# Creates the API - Javascript calls the API directly
resource "aws_api_gateway_rest_api" "web_api_gateway" {
  name = "${var.api_gateway_name}"

  endpoint_configuration {
    types = ["EDGE"]
  }
}
# Adds an API authoriser  - Enables Cognito to control access to the API
resource "aws_api_gateway_authorizer" "web_api_gateway_auth" {
  name                   = "WildRydes"
  rest_api_id            = "${aws_api_gateway_rest_api.web_api_gateway.id}"
  authorizer_uri         = "${aws_lambda_function.unicorn_request.invoke_arn}"
  authorizer_credentials = "${aws_iam_role.iam_for_lambda.arn}"
  type                   = "COGNITO_USER_POOLS"
  provider_arns          = ["${aws_cognito_user_pool.pool.arn}"] 
}
# Adds the http resource - this is where the http methods live
resource "aws_api_gateway_resource" "rides_resource" {
  rest_api_id = "${aws_api_gateway_rest_api.web_api_gateway.id}"
  parent_id   = "${aws_api_gateway_rest_api.web_api_gateway.root_resource_id}"
  path_part   = "ride"
}
# Adds the POST request method with Cognito authorisation to the API - If authorised this enables the browser
# to send data to the API via a http POST
resource "aws_api_gateway_method" "post_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.web_api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.rides_resource.id}"
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = "${aws_api_gateway_authorizer.web_api_gateway_auth.id}"
}
# Defines the target backend that the POST method will call and whether the incoming request data should be 
# modified. In this example the AWS_PROXY means it's a Lambda integration with the URI as the Lambda function
resource "aws_api_gateway_integration" "post_integration" {
  rest_api_id             = "${aws_api_gateway_rest_api.web_api_gateway.id}"
  resource_id             = "${aws_api_gateway_resource.rides_resource.id}"
  http_method             = "${aws_api_gateway_method.post_method.http_method}"
  integration_http_method = "${aws_api_gateway_method.post_method.http_method}"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.unicorn_request.invoke_arn}"
}
# Adds the OPTION request method - This is required for CORS browser security policies. It means the browser 
# has to send a preflight request to the server and wait for approval (or a request for credentials) from the 
# server before sending the actual request. This request appears to your API as a http request that has an ORIGIN header
resource "aws_api_gateway_method" "options_method" {
  rest_api_id   = "${aws_api_gateway_rest_api.web_api_gateway.id}"
  resource_id   = "${aws_api_gateway_resource.rides_resource.id}"
  http_method   = "OPTIONS"
  authorization = "NONE"
  authorizer_id = "${aws_api_gateway_authorizer.web_api_gateway_auth.id}"
}
# Adds the method response headers for the OPTIONS method
resource "aws_api_gateway_method_response" "options_200" {
    rest_api_id   = "${aws_api_gateway_rest_api.web_api_gateway.id}"
    resource_id   = "${aws_api_gateway_resource.rides_resource.id}"
    http_method   = "${aws_api_gateway_method.options_method.http_method}"
    status_code   = "200"
    response_models = {
        "application/json" = "Empty"
    }
    response_parameters = {
        "method.response.header.Access-Control-Allow-Headers" = true,
        "method.response.header.Access-Control-Allow-Methods" = true,
        "method.response.header.Access-Control-Allow-Origin" = true
    }
    depends_on = ["aws_api_gateway_method.options_method"]
}
# Adds the integration response for the OPTIONS method. The response parameteres are the mappings that are key to CORS functioning.
resource "aws_api_gateway_integration_response" "options_integration_response" {
    rest_api_id   = "${aws_api_gateway_rest_api.web_api_gateway.id}"
    resource_id   = "${aws_api_gateway_resource.rides_resource.id}"
    http_method   = "${aws_api_gateway_method.options_method.http_method}"
    status_code   = "${aws_api_gateway_method_response.options_200.status_code}"
    response_parameters = {
        "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
        "method.response.header.Access-Control-Allow-Methods" = "'OPTIONS,POST'",
        "method.response.header.Access-Control-Allow-Origin" = "'*'"
    }
    depends_on = ["aws_api_gateway_method_response.options_200"]
}
# Defines the target backend that the Options method will call and whether the incoming request data should be 
# modified. In this example the MOCK means it returns a response purely using API Gateway mappings and transformations.
resource "aws_api_gateway_integration" "options_integration" {
  rest_api_id             = "${aws_api_gateway_rest_api.web_api_gateway.id}"
  resource_id             = "${aws_api_gateway_resource.rides_resource.id}"
  http_method             = "${aws_api_gateway_method.options_method.http_method}"
  type                    = "MOCK"
  request_templates       = {
"application/json" = <<EOF
{ "statusCode": 200 }
EOF
  }
  depends_on = ["aws_api_gateway_method.options_method"]
}
# Gives API Gateway permission to invoke the Lambda function 
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.unicorn_request.function_name}"
  principal     = "apigateway.amazonaws.com"
}
# A stage is a named reference to a deployment, which is a snapshot of the API. You use a Stage to manage and optimize a particular deployment.
resource "aws_api_gateway_deployment" "deploy_rides" {
  rest_api_id = "${aws_api_gateway_rest_api.web_api_gateway.id}"
  stage_name  = "prod"

  depends_on = ["aws_api_gateway_integration_response.options_integration_response"]
}