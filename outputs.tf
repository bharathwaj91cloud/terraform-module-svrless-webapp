output "s3_bucket_name" {
  value       = aws_s3_bucket.hostbucket.bucket
  description = "The name of the S3 bucket"
}
output "s3_bucket_arn" {
  value       = aws_s3_bucket.hostbucket.arn
  description = "The arn of the S3 bucket"
}
output "s3_bucket_endpoint" {
  value       = aws_s3_bucket.hostbucket.website_endpoint
  description = "The endpoint for accessing the S3 bucket"
}
output "cog_usrpool_id" {
  value       = aws_cognito_user_pool.pool.id
  description = "The id of the Cognito user sign-in pool"
}
output "cog_usrpool_arn" {
  value       = aws_cognito_user_pool.pool.arn
  description = "The arn of the Cognito user sign-in pool"
}
output "cog_usrpool_clientid" {
  value       = aws_cognito_user_pool_client.client.id
  description = "The is of the Cognito user sign-in client"
}
output "dynamodb_arn" {
  value       = aws_dynamodb_table.rides.arn
  description = "The arn of the DynamoDB table"
}
output "API_gateway_invoke_url" {
  value       = aws_api_gateway_deployment.deploy_rides.invoke_url
  description = "The invoke URL of the API"
}