# Adds the database table to record the requested rides
resource "aws_dynamodb_table" "rides" {
  name         = "${var.dynamodb_table_name}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "RideId" 

  attribute {
    name = "RideId"
    type = "S"
  } 
}