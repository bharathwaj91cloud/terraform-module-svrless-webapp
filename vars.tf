variable "AWS_REGION" {
  default = "eu-west-2"
}
variable "cognito_userpool_name" {
  description = "name of user sign-in pool"
  type = string
}
variable "cognito_client_name" {
  description = "name of user sign-in client"
  type = string
}
variable "s3_bucket_name" {
  description = "name of public bucket to host website"
  type = string
}
variable "dynamodb_table_name" {
  description = "name of dynamodb table for rides"
  type = string
}
variable "aws_iam_role_name" {
  description = "name of the Lambda IAM role"
  type = string
}
variable "lambda_function_name" {
  description = "name of the Lambda function"
  type = string
}
variable "api_gateway_name" {
  description = "name of the API gateway"
  type = string
}

